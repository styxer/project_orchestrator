use std::{
    io::stderr,
    panic,
    ops::{Deref, DerefMut}
};

use ratatui::prelude::Frame;

use crate::EventHandler;

use crossterm::{
    terminal::{
        disable_raw_mode,
        enable_raw_mode,
        EnterAlternateScreen,
        LeaveAlternateScreen,
    },
    event::{
        EnableMouseCapture,
        DisableMouseCapture
    },
};

use ratatui::backend::CrosstermBackend as Backend;

use anyhow::Result;

pub type CrosstermTerminal = ratatui::Terminal<ratatui::backend::CrosstermBackend<std::io::Stderr>>;

pub struct Tui {
    terminal: CrosstermTerminal,
    pub event_handler: EventHandler
}

impl Tui {
    pub fn new(terminal: CrosstermTerminal, event_handler: EventHandler) -> Self {
        Self {terminal, event_handler}
    }

    pub fn initialize(&mut self) -> Result<()> {
        enable_raw_mode()?;
        crossterm::execute!(stderr(), EnterAlternateScreen, EnableMouseCapture)?;

        let panic_hook = panic::take_hook();
        panic::set_hook(Box::new(move |panic| {
          Self::reset().expect("failed to reset the terminal");
          panic_hook(panic);
        }));

        self.terminal.hide_cursor()?;
        self.terminal.clear()?;

        Ok(())
    }

    fn reset() -> Result<()> {
        disable_raw_mode()?;
        crossterm::execute!(stderr(), LeaveAlternateScreen, DisableMouseCapture)?;
        Ok(())
    }

    pub fn quit(&mut self) -> Result<()> {
        Self::reset()?;
        self.terminal.show_cursor()?;

        Ok(())
    }
}

impl Deref for Tui {
  type Target = ratatui::Terminal<Backend<std::io::Stderr>>;

  fn deref(&self) -> &Self::Target {
    &self.terminal
  }
}

impl DerefMut for Tui {
  fn deref_mut(&mut self) -> &mut Self::Target {
    &mut self.terminal
  }
}

impl Drop for Tui {
  fn drop(&mut self) {
    self.quit().unwrap();
  }
}
