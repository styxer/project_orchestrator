use serde::{Deserialize, Serialize};
use std::{
    collections,
    fs::File,
    process::Stdio,
    sync::{Arc, Mutex},
};

use tokio::io::{AsyncBufReadExt, BufReader};
use tokio::process::Command;
use tokio::task::JoinHandle;

#[derive(Serialize, Deserialize, Debug, Clone)]
struct ProcessConfig {
    executable_path: String,
    commandline_arguments: Vec<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct Config {
    processes: collections::HashMap<String, ProcessConfig>,
}

/// Terminal events.
#[derive(Clone, Copy, Debug)]
pub enum Action {
    Error,
    Tick,
    Quit,
    Render,
    IncrementSelectedProcessIndex,
    DecrementSelectedProcessIndex,
    StopSelectedProcess,
}

/// Application.
#[derive(Debug)]
pub struct App {
    /// should the application exit?
    pub should_quit: bool,
    selected_process_index: usize,
    config: Config,
    process_stdout: Arc<Mutex<collections::HashMap<String, Vec<String>>>>,
    process_names: Vec<String>,
    running_threads: Arc<Mutex<collections::HashMap<String, JoinHandle<()>>>>,

    pub sender: tokio::sync::mpsc::UnboundedSender<Action>,
    pub receiver: tokio::sync::mpsc::UnboundedReceiver<Action>,
}

impl App {
    pub fn new(config_path: String) -> Self {
        let file = File::open(config_path.clone())
            .expect(format!("Could not open file '{}'", config_path.clone()).as_str());

        let reader = std::io::BufReader::new(file);
        let config: Config = serde_json::from_reader(reader).expect("JSON was not well-formatted");

        let process_names: Vec<String> = config
            .processes
            .keys()
            .map(|element| element.clone())
            .collect();

        let (sender, mut receiver) = tokio::sync::mpsc::unbounded_channel();

        Self {
            should_quit: false,
            selected_process_index: 0,
            config,
            process_stdout: Arc::new(Mutex::new(collections::HashMap::from_iter(
                process_names
                    .iter()
                    .map(|element| (element.clone(), Vec::new()))
                    .collect::<Vec<_>>(),
            ))),
            process_names: process_names
                .iter()
                .map(|element| element.clone())
                .collect(),
            running_threads: Arc::new(Mutex::new(collections::HashMap::new())),
            sender,
            receiver,
        }
    }

    async fn spawn_process(
        process_output: Arc<Mutex<collections::HashMap<String, Vec<String>>>>,
        process_name: String,
        executable_path: String,
        commandline_arguments: Vec<String>,
    ) {
        let mut command = Command::new(executable_path);
        for arg in commandline_arguments {
            command.arg(arg);
        }

        command.stdout(Stdio::piped());
        command.kill_on_drop(true);

        let mut child = command.spawn().expect("failed to spawn command");

        let stdout = child
            .stdout
            .take()
            .expect("child did not have a handle to stdout");

        let mut reader = BufReader::new(stdout).lines();
        while let Some(line) = reader.next_line().await.unwrap() {
            let mut process_stdout = process_output.lock().unwrap();
            let stdout_vec = process_stdout.get_mut(&process_name).unwrap();

            stdout_vec.push(line);
        }
    }

    pub fn run(&self) {
        for process in self.config.clone().processes {
            let process_output = self.process_stdout.clone();
            let running_threads = self.running_threads.clone();
            let cloned_process = process.clone();

            let join_handle = tokio::spawn(async move {
                let process_name = cloned_process.0;
                let executable_path = cloned_process.1.executable_path;
                let commandline_arguments = cloned_process.1.commandline_arguments;

                App::spawn_process(
                    process_output,
                    process_name.clone(),
                    executable_path,
                    commandline_arguments,
                )
                .await;
            });

            running_threads
                .lock()
                .unwrap()
                .insert(process.0, join_handle);
        }
    }

    pub fn quit(&mut self) {
        self.should_quit = true;
    }

    pub fn process_names(&self) -> &Vec<String> {
        &self.process_names
    }

    pub fn increment_selected_process_index(&mut self) {
        self.selected_process_index += 1;
        self.selected_process_index %= self.config.processes.len();
    }

    pub fn decrement_selected_process_index(&mut self) {
        if self.selected_process_index == 0 {
            self.selected_process_index = self.config.processes.len() - 1;
        } else {
            self.selected_process_index -= 1;
        }
    }

    pub fn selected_process_index(&self) -> usize {
        self.selected_process_index
    }

    pub fn selected_process_output(&mut self, process_index: usize) -> Vec<String> {
        let process_stdout = self.process_stdout.lock().unwrap();
        process_stdout
            .get(&self.process_names[process_index])
            .unwrap()
            .to_owned()
    }

    pub fn selected_process_output_len(&mut self, process_index: usize) -> usize {
        let process_stdout = self.process_stdout.lock().unwrap();
        process_stdout
            .get(&self.process_names[process_index])
            .unwrap()
            .len()
    }

    pub fn selected_process_arguments(&self, process_index: usize) -> String {
        self.config
            .processes
            .get(&self.process_names[process_index])
            .unwrap()
            .commandline_arguments
            .join(" ")
    }

    pub fn start_or_stop_selected_process(&self) {
        let current_selected_process = self.process_names[self.selected_process_index].clone();
        let locked_running_threads = self.running_threads.lock().unwrap();

        if locked_running_threads
            .get(&current_selected_process)
            .is_none()
        {
            return;
        }

        // TODO: Cancelling a thread was never a good idea :) Implement tokio sender/receiver
        // pattern
        self.running_threads
            .lock()
            .unwrap()
            .get_mut(&current_selected_process)
            .unwrap()
            .abort();

        self.running_threads
            .lock()
            .unwrap()
            .remove(&current_selected_process);
    }
}
