use crossterm::event::{KeyCode, KeyEventKind};

use ratatui::prelude::CrosstermBackend;
use ratatui::Terminal;
use std::io::stderr;

use anyhow::Result;

use clap::Parser;

pub mod app;
pub mod event;
pub mod tui;
pub mod ui;

use app::{App, Action};
use event::{Event, EventHandler};
use tui::Tui;

/// ProcessOrchestrator - Starts the configured programs within the given config file
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Path to the config file
    #[arg(short, long)]
    config_path: String,
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();

    let mut app = App::new(args.config_path);
    let event_handler = EventHandler::new(1000, 8.0);

    let terminal = Terminal::new(CrosstermBackend::new(stderr()))?;
    let mut tui = Tui::new(terminal, event_handler);

    tui.initialize()?;
    app.run();

    loop {
        let event = tui.event_handler.next().await.unwrap();
        match event {
            Event::Key(key) => match key.kind {
                KeyEventKind::Press => match key.code {
                    KeyCode::Char('q') => app.sender.send(Action::Quit)?,
                    KeyCode::Char('j') => app.sender.send(Action::IncrementSelectedProcessIndex)?,
                    KeyCode::Char('k') => app.sender.send(Action::DecrementSelectedProcessIndex)?,
                    KeyCode::Char('s') => app.sender.send(Action::StopSelectedProcess)?,
                    _ => {}
                },
                _ => {}
            },
            Event::Render => {
                app.sender.send(Action::Render)?;
            }
            _ => {}
        }

        while let Ok(action) = app.receiver.try_recv() {
            match action {
                Action::Quit => app.quit(),
                Action::IncrementSelectedProcessIndex => app.increment_selected_process_index(),
                Action::DecrementSelectedProcessIndex => app.decrement_selected_process_index(),
                Action::StopSelectedProcess => app.start_or_stop_selected_process(),
                _ => {}
            }

            if let Action::Render = action {
                tui.draw(|frame| { ui::render(frame, &mut app) })?;
            }
        }

        if app.should_quit {
            break;
        }
    }

    tui.quit()?;
    Ok(())
}
