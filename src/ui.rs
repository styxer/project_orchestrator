use ratatui::{
    prelude::{Alignment, Constraint, Direction, Frame, Layout},
    style::{Color, Modifier, Style},
    widgets::{Block, BorderType, Borders, List, ListItem, ListState, Paragraph},
};

use crate::app::App;

pub fn render<B: ratatui::backend::Backend>(f: &mut Frame<B>, app: &mut App) {
    let area = f.size();
    let layout = Layout::default()
        .direction(Direction::Horizontal)
        .constraints(vec![Constraint::Percentage(20), Constraint::Percentage(80)])
        .split(area);

    let layout_2 = Layout::default()
        .direction(Direction::Vertical)
        .constraints(vec![Constraint::Percentage(20), Constraint::Percentage(80)])
        .split(layout[1]);

    let list_elements: Vec<ListItem> = app
        .process_names()
        .iter()
        .map(|element| ListItem::new(element.to_owned()))
        .collect();

    let mut state = ListState::default().with_selected(Some(app.selected_process_index()));

    f.render_stateful_widget(
        List::new(list_elements)
            .block(
                Block::default()
                    .title("Process List")
                    .title_alignment(Alignment::Center)
                    .borders(Borders::ALL)
                    .border_type(BorderType::Plain),
            )
            .style(Style::default().fg(Color::White))
            .highlight_style(
                Style::default()
                    .add_modifier(Modifier::BOLD)
                    .fg(Color::Black)
                    .bg(Color::White),
            ),
        layout[0],
        &mut state,
    );

    f.render_widget(
        Paragraph::new(app.selected_process_arguments(state.selected().unwrap()))
            .alignment(Alignment::Left)
            .block(
                Block::default()
                    .title("Process Arguments")
                    .title_alignment(Alignment::Center)
                    .borders(Borders::ALL)
                    .border_type(BorderType::Plain),
            ),
        layout_2[0],
    );

    let app_stdout = app
        .selected_process_output(state.selected().unwrap())
        .iter()
        .map(|element| ListItem::new(element.to_owned()))
        .collect::<Vec<_>>();

    let stdout_size = app.selected_process_output_len(state.selected().unwrap());
    let mut stdout_state = ListState::default().with_selected(Some(stdout_size));

    f.render_stateful_widget(
        List::new(app_stdout)
            .block(
                Block::default()
                    .title("Process Stdout")
                    .title_alignment(Alignment::Center)
                    .borders(Borders::ALL)
                    .border_type(BorderType::Plain),
            ),
        layout_2[1],
        &mut stdout_state
    );
}
