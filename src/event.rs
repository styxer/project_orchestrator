use color_eyre::eyre::Result;
use crossterm::event::KeyEvent;
use futures::{FutureExt, StreamExt};

/// Terminal events.
#[derive(Clone, Copy, Debug)]
pub enum Event {
  Error,
  Tick,
  Render,
  Key(KeyEvent),
}

/// Terminal event handler.
#[derive(Debug)]
pub struct EventHandler {
  _sender: tokio::sync::mpsc::UnboundedSender<Event>,
  /// Event receiver channel.
  receiver: tokio::sync::mpsc::UnboundedReceiver<Event>,
  task: Option<tokio::task::JoinHandle<()>>,
}

impl EventHandler {
  /// Constructs a new instance of [`EventHandler`].
  pub fn new(tick_rate: u64, render_rate: f64) -> Self {
    let tick_rate = std::time::Duration::from_millis(tick_rate);
    let render_rate = std::time::Duration::from_secs_f64(1.0 / render_rate);

    let (sender, receiver) = tokio::sync::mpsc::unbounded_channel();
    let _sender = sender.clone();

    let task = tokio::spawn(async move {
      let mut reader = crossterm::event::EventStream::new();
      let mut tick_interval = tokio::time::interval(tick_rate);
      let mut render_interval = tokio::time::interval(render_rate);
      loop {
        let tick_delay = tick_interval.tick();
        let render_delay = render_interval.tick();

        let crossterm_event = reader.next().fuse();

        tokio::select! {
          maybe_event = crossterm_event => {
            match maybe_event {
              Some(Ok(evt)) => {
                match evt {
                  crossterm::event::Event::Key(key) => {
                    if key.kind == crossterm::event::KeyEventKind::Press {
                      let _ = sender.send(Event::Key(key));
                    }
                  },
                  _ => {},
                }
              }
              Some(Err(_)) => {
                let _ = sender.send(Event::Error);
              }
              None => {},
            }
          },
          _ = tick_delay => {
              let _ = sender.send(Event::Tick);
          },
          _ = render_delay => {
              let _ = sender.send(Event::Render);
          },
        }
      }
    });

    Self { _sender, receiver, task: Some(task) }
  }

  /// Receive the next event from the handler thread.
  ///
  /// This function will always block the current thread if
  /// there is no data available and it's possible for more data to be sent.
  pub async fn next(&mut self) -> Result<Event> {
    self.receiver.recv().await.ok_or(color_eyre::eyre::eyre!("Unable to get event"))
  }
}
